var express = require('express')
var router = express.Router()
const mongodb = require('mongodb');

const DB_URI = 'mongodb://localhost:27017';
const MongoClient = mongodb.MongoClient;
const client = new MongoClient(DB_URI, { useNewUrlParser: true, useUnifiedTopology: true });

const DB_NAME = 'bookstore';
const BOOK_COLLECTION_NAME = "orders";

router.get('/', function(req, res) {

  client.connect(function(err, connection){
    const db = connection.db(DB_NAME); // connecting to the book store database
    db.collection(ORDER_COLLECTION_NAME)
      .find({})
      .toArray(function(find_err, records){
        if (find_err)
          return res.status(500).send(find_err);

        return res.send(records);
      });
  });
})

router.post('/', function(req, res){

  if (!req.body || req.body.length === 0)
    return res.status(400).send({"message": "record is required"});

  // data validation
  if (!req.body.user_id || !req.body.book_ids || !req.body.book_ids.length > 0 || !req.body.total_price || !req.body.shipping_address)
    res.status(400).send({
      message: "user_id, book_ids, total_price, and shipping_address are required"
    });

  // date is in req.body
  client.connect(function(err, connection){
    const db = connection.db(DB_NAME);

    db.collection(ORDER_COLLECTION_NAME)
      .insertOne(req.body, function(insert_error, data){
        if (insert_error)
          return res.status(500).send({message: "Something went wrong"});

        connection.close();

        return res.status(200).send({message: "Record inserted successfully"});
      });
  });
})

router.put('/:id', function(req, res) {
  client.connect(function(err, connection) {
    if (err)
      return res.status(500).send({error: err});

    const db = connection.db(DB_NAME);
    db.collection(ORDER_COLLECTION_NAME)
      .updateOne({_id: ObjectID(req.params.id)},{$set: req.body}, function(update_err, update_data){
         if (update_err)
          return res.status(500).send({error: update_err, message: "Could not update the record"});

        return res.status(200).send({message: "Update was successful!", data: update_data});
      });
  });
})

module.exports = router
