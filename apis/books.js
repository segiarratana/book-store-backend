var express = require('express')
var router = express.Router()
const mongodb = require('mongodb');

const DB_URI = 'mongodb://localhost:27017';
const MongoClient = mongodb.MongoClient;
const client = new MongoClient(DB_URI, { useNewUrlParser: true, useUnifiedTopology: true });

const DB_NAME = 'bookstore';
const BOOK_COLLECTION_NAME = "books";

router.get('/', function(req, res) {

  client.connect(function(err, connection){
    if(err)
    {
      console.log(err)
      return res.status(500).send(err);
    }
    const db = connection.db(DB_NAME); // connecting to the book store database
    db.collection(BOOK_COLLECTION_NAME)
      .find({})
      .toArray(function(find_err, records){
        if (find_err)
          return res.status(500).send(find_err);

        return res.send(records);
      });
  });
})

router.post('/', function(req, res){

  if (!req.body || req.body.length === 0)
    return res.status(400).send({"message": "record is required"});

  console.log(req.body);
  // data validation
  if (!req.body.title || !req.body.author || !req.body.price || !req.body.isbn)
    return res.status(400).send({
      message: "Title, author, price, and isbn are required"
    });

  // date is in req.body
  client.connect(function(err, connection){
    const db = connection.db(DB_NAME);

    db.collection(BOOK_COLLECTION_NAME)
      .insertOne(req.body, function(insert_error, data){
        if (insert_error)
          return res.status(500).send({message: "Something went wrong"});

        connection.close();

        return res.status(200).send({message: "Record inserted successfully"});
      });
  });
})

router.delete('/', function(req, res) {
  db.collection(COLLECTION_NAME)
    .deleteOne({"_id": objectId(req.body.id)}, function(error, data){
    if (error)
      return res.status(500).send(error);
    return res.status(200).send('Book has been successfully deleted.');
  });
});

module.exports = router
